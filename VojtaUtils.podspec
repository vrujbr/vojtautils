#
# Be sure to run `pod lib lint VojtaUtils.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'VojtaUtils'
  s.version          = '1.2.2'
  s.summary          = 'Simple every-project Vojtas utils and extensions.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
All the extensions and utils I create with new projects again and again.
                       DESC

  s.homepage         = 'https://bitbucket.org/vrujbr/vojtautils'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Vojta' => 'vrujbr@gmail.com' }
  s.source           = { :git => 'https://vrujbr@bitbucket.org/vrujbr/vojtautils.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'VojtaUtils/Classes/**/*'
  
  # s.resource_bundles = {
  #   'VojtaUtils' => ['VojtaUtils/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'RxSwift'
  s.dependency 'RxCocoa'
  s.dependency 'SwiftyBeaver'
  s.dependency 'SnapKit'
end
