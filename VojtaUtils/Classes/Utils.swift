//
//  Utils.swift
//  Pods
//
//  Created by Vojta on 2/4/17.
//
//

import SwiftyBeaver

public let logger: SwiftyBeaver.Type = {
    let logger = SwiftyBeaver.self
    return logger
}()



@IBDesignable public class TopAlignedLabel: UILabel {
    public override func drawText(in rect: CGRect) {
        if let stringText = text {
            let stringTextAsNSString = stringText as NSString
            let labelStringSize = stringTextAsNSString.boundingRect(with: CGSize(width: self.frame.width,height: CGFloat.greatestFiniteMagnitude),
                                                                    options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                                    attributes: [NSAttributedString.Key.font: font!],
                                                                    context: nil).size
            super.drawText(in: CGRect(x:0,y: 0,width: self.frame.width, height:ceil(labelStringSize.height)))
        } else {
            super.drawText(in: rect)
        }
    }
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        layer.borderWidth = 1
        layer.borderColor = UIColor.black.cgColor
    }
}

public extension Double {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}

public func mod<T: BinaryInteger>(_ a: T, _ n: T) -> T {
    precondition(n > 0, "modulus must be positive")
    let r = a % n
    return r >= 0 ? r : r + n
}
