//
//  OtherExtensions.swift
//  Pods
//
//  Created by Vojta on 2/4/17.
//
//

import SwiftyBeaver

public extension UIColor {
    convenience init(_ red: Int, _ green: Int, _ blue: Int) {
        let newRed = CGFloat(red)/255.0
        let newGreen = CGFloat(green)/255.0
        let newBlue = CGFloat(blue)/255.0
        
        self.init(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0)
    }
}

public extension SwiftyBeaver.Level {
    /**
     Increase the severity of according log levelpublic extension String {
     public func attributed(font: UIFont, color: UIColor) -> NSAttributedString {
     return NSAttributedString(string: self, attributes: [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: color])
     }
     }
     */
    func increase() -> SwiftyBeaver.Level {
        //severe is the maximum, do not return .None level
        guard self == SwiftyBeaver.Level.error else {
            return self
        }
        guard let newLog = SwiftyBeaver.Level(rawValue: self.rawValue+1) else {
            return self
        }
        return newLog
    }
    
    /**
     Decrease the severity of according log level
     */
    func decrease() -> SwiftyBeaver.Level {
        guard let newLog = SwiftyBeaver.Level(rawValue: self.rawValue-1) else {
            return self
        }
        return newLog
    }
}

public extension String {
    func attributed(font: UIFont, color: UIColor) -> NSAttributedString {
        return NSAttributedString(string: self, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color])
    }
}
