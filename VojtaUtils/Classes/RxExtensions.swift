//
//  Utilities.swift
//  BoozeBuzzer
//
//  Created by Vojtěch Rujbr on 11.07.16.
//  Copyright © 2016 Vojtěch Rujbr. All rights reserved.
//

import SwiftyBeaver
import RxSwift
import RxCocoa
import SnapKit

public extension Observable {
    func subscribeNext(closure: @escaping ((_ element: Element) -> Void)) -> Disposable {
        return self.subscribe { event in
            switch event {
            case .next(let element):
                closure(element)
            default: break
            }
        }
    }
}

public protocol OptionalEquivalent {
    associatedtype Wrapped
    var value: Wrapped? { get }
}
extension Optional: OptionalEquivalent {
    
    // just to cast `Optional<Wrapped>` to `Wrapped?`
    public var value: Wrapped? {
        return self
    }
}
public extension Observable where Element: OptionalEquivalent {
    //Filteres the observable for nil values and unwrapes the rest
    func filterAndUnwrap() -> Observable<Element.Wrapped> {
        return self.map{$0.value}.filter{$0 != nil}.map{$0!}
    }
}



public extension ControlEvent {
    func subscribeNext(closure: @escaping ((_ element: Element) -> Void)) -> Disposable {
        return self.subscribe { event in
            switch event {
            case .next(let element):
                closure(element)
            default: break
            }
        }
    }
}



///Beaver debug
public extension ObservableType {
    func beaverDebug(_ level: SwiftyBeaver.Level, identifier: String, customPrint: ((Self.Element) -> String)? = nil) -> Observable<Self.Element> {
        return Observable.create { observer in
            
            logger.custom(level: level.decrease(), message: "subscribed \(identifier)")
            let subscription = self.subscribe { event in
                if case let .next(element) = event,
                    let customPrint = customPrint {
                    logger.custom(level: level, message: "event \(identifier) next \(customPrint(element))")
                } else {
                    let eventStr = "\(event)"
                    let trimStr = eventStr.prefix(100)
                    logger.custom(level: level, message: "event \(identifier) \(trimStr)...")
                }
                observer.on(event) //forward the stream
            }
            return Disposables.create(with: {
                logger.custom(level: level.decrease(), message: "disposing \(identifier)")
                subscription.dispose()
            })
        }
    }
}
public extension SharedSequence {
    func beaverDebug(_ level: SwiftyBeaver.Level, identifier: String, customPrint: ((Element) -> String)? = nil) -> Driver<Element> {
        return self.asObservable()
            .beaverDebug(level, identifier: identifier, customPrint: customPrint)
            .asDriver {error in
                logger.warning("Beaver debug on driver failed with error \(error)")
                return Driver<Element>.empty()
            }
    }
}


//SnapKip
extension Constraint: ReactiveCompatible { }
extension Reactive where Base: Constraint {
    
    /// Bindable sink for setting contraint active or deactive.
    public var isActivated: Binder<Bool> {
        return Binder(self.base) { control, value in
            switch value {
            case true:
                control.activate()
            case false:
                control.deactivate()
            }
        }
    }
}

/*
//Variable
extension Variable: ReactiveCompatible { }
extension Reactive where Base: Variable<Bool> {
    
    /// Bindable sink for toggling bool variable value
    public var toggle: Binder<()> {
        return Binder(self.base) { variable, _ in
            variable.value = !variable.value
        }
    }
}*/

public extension Observable {
    ///Throws error if condition is true
    func throwOn(condition: @escaping (Element) -> Bool) -> Observable<Element> {
        return flatMap { (value: Element) -> Observable<Element> in
            if condition(value) {
                return Observable<Element>.error(NSError(domain: "Observable", code: 10340, userInfo: [NSLocalizedDescriptionKey: "Observable threw on condition (VojtaUtils)"]))
            } else {
                return self
            }
        }
    }
}


