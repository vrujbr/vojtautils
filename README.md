# VojtaUtils

[![CI Status](http://img.shields.io/travis/Vojta/VojtaUtils.svg?style=flat)](https://travis-ci.org/Vojta/VojtaUtils)
[![Version](https://img.shields.io/cocoapods/v/VojtaUtils.svg?style=flat)](http://cocoapods.org/pods/VojtaUtils)
[![License](https://img.shields.io/cocoapods/l/VojtaUtils.svg?style=flat)](http://cocoapods.org/pods/VojtaUtils)
[![Platform](https://img.shields.io/cocoapods/p/VojtaUtils.svg?style=flat)](http://cocoapods.org/pods/VojtaUtils)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

VojtaUtils is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "VojtaUtils"
```

## Author

Vojta, vrujbr@gmail.com

## License

VojtaUtils is available under the MIT license. See the LICENSE file for more info.
